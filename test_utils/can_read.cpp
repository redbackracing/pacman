#include <linux/can.h>
#include <linux/can/raw.h>

#include <sys/ioctl.h>
#include <sys/socket.h>

#include <libsocketcan.h>
#include <unistd.h>
#include <stdio.h>

#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <cerrno>


#define ONE_MEGABIT 1000000

int main(int argc, char *argv[]) {

	/* 
	TODO: get status of interface before
	trying to start can. Otherwise prorgam will break
	*/
	

	/* set bitrate */
	if (can_set_bitrate("can0", ONE_MEGABIT)) {
		std::cout << "Error setting bitrate: " << std::strerror(errno) << std::endl;
	}

	if (can_do_start("can0")) {
		std::cout << "Error starting the CAN interface" << std::endl;
	}

	
	int s = socket(PF_CAN, SOCK_RAW, CAN_RAW);

	if (s == -1 ) {
		std::cout << std::strerror(errno) << std::endl;
	}


	struct sockaddr_can addr;

	addr.can_family = AF_CAN;
	/* bind to all and any can interfaces */
	addr.can_ifindex = 0;
	
	bind(s, reinterpret_cast<struct sockaddr *>(&addr), sizeof(addr));

	struct can_frame frame;

	int bytes_read = read(s, &frame, sizeof(struct can_frame));

	if (bytes_read < 0) {
		std::cout << "Error reading from socket: " << std::strerror(errno) << std::endl;
	}

	if (bytes_read < sizeof(struct can_frame)) {
		std::cerr << "Incomplete CAN frame was read" << std::endl;
	}

	std::cout << "Can frame recieved, read " << bytes_read << " bytes" << std::endl;

	
	struct timeval timestamp;
	while (true) {
		int bytes_read = read(s, &frame, sizeof(struct can_frame));
		/* get the timestamp of the can frame reception */
		ioctl(s, SIOCGSTAMP, &timestamp); 

		std::stringstream can_out;
		can_out << "identifier: " <<  frame.can_id << std::endl;
		can_out << "bytes of data recieved " << unsigned(frame.can_dlc) << std::endl;
		
		for (int i=0; i<frame.can_dlc; i++) {
			can_out << unsigned(frame.data[i]) << " ";
		}
		
		std::cout << std::dec << can_out.str() << std::endl;
		printf("%ld.%06ld\n", timestamp.tv_sec, timestamp.tv_usec);
	}

	
}
