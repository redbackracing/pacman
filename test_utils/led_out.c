/*
 * This test application is to read/write data directly from/to the device 
 * from userspace. 
 * 
 */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

#define DRIVER_ADDR 0x40000000

int main(int argc, char *argv[])
{
	int fd;
	int value = 0;
	
	unsigned page_addr, page_offset;
	void *ptr;
	unsigned page_size=sysconf(_SC_PAGESIZE);

	
	/* Open /dev/mem file */
	fd = open ("/dev/mem", O_RDWR);
	if (fd < 1) {
		perror(argv[0]);
		return -1;
	}

	/* mmap the device into memory */
	page_addr = (DRIVER_ADDR & (~(page_size-1)));
	page_offset = DRIVER_ADDR - page_addr;
	printf("page addr is: %x", page_addr);
	printf("page offset is: %x", page_offset);
	ptr = mmap(NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr);

	/* Read value from the device register */
	/*
	value = *((unsigned *)(ptr + page_offset));
	printf("gpio dev-mem test: input: %08x\n",value);
	*/

	/* Write value to the device register */
	uint32_t *driver_dev = ((uint32_t*)(ptr + page_offset)); 
	*driver_dev = 10;
	for (int i=1; i<10; i++) {
		*(driver_dev+i) = 0xFFFFFFFF;
	}

	//*((unsigned *)(ptr + page_offset)) = value;
	

	munmap(ptr, page_size);

	return 0;
}

