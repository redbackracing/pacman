
#include <Canbus.h>
#include <defaults.h>
#include <global.h>
#include <mcp2515.h>
#include <mcp2515_defs.h>
#include <stdarg.h>



#define SENSORS_PER_FRAME 4
/* this field is configurable */
#define SENSORS_IN_SYSTEM 4

const bool debug = false;
int frames_required; 
unsigned int counter = 0;
unsigned int counter_gear = 0;


void setup() {
  delay(1000);
  Serial.begin(9600);
  Serial.println("M800 CAN traffic emulation\n\n");
  // TODO: some ascii art 

  Serial.println("Initialising CAN");
  if(Canbus.init(CAN_1000KBPS)) {
    Serial.println("CAN initialised. All is well. :)");
  } else {
    Serial.println("Something went wrong with the initialisation...");
  }

  
  /*  setting the normal mode operation see
      https://imgur.com/a/QQF3d 
  */
  mcp2515_bit_modify(CANCTRL, (1<<REQOP2)|(1<<REQOP1)|(1<<REQOP0), 0);
  

  frames_required = ceiling( SENSORS_IN_SYSTEM, SENSORS_PER_FRAME );

  debug_print("Number of frames required is %d",frames_required);
  delay(1000);
}

/* x is dividend, y is divisor */
int ceiling(int x, int y) {
  int q = (x + y - 1) / y;
  return q;
}

void loop() {
  if ( (counter % 1500) == 0) {
    counter_gear++;
  }
  
  if (counter == 11000) {
    counter = 0;
    counter_gear = 0;
  }

  
  // uint8_t data[8] = rand_sensor_data();
  // Serial.println(data);
  // put your main code here, to run repeatedly:
  tCAN can_msg;
  for (int frame=0; frame<frames_required; frame++) {
    /* this emulates the sequention sending of can frames
     *  see doucmentation/README for more details.
     */
    can_msg.id = 500 + frame;
    /* 'Remote Transmission Request'
     *  when 0, shows that this is a normal data frame
     *  when 1, shows it is a request frame
     */
    can_msg.header.rtr = 0;
    /* this is the data length code (DLC)...i think
     *  i.e. the number of bytes of data 0-8
     */
    can_msg.header.length = 8;
    
    /* generate some sensor data */
    rand_sensor_data(&can_msg.data[0]);
    
    for (int i=0; i<8; i++) {
      debug_print("[%d]:%d ", i, can_msg.data[i]);
    }
    debug_print("\n");
    
    mcp2515_send_message(&can_msg);

    // delay(1000);
  }


}

/* returns an 8 byte array containing random
 *  data to emulate sensor data.
 */
void rand_sensor_data(uint8_t sensor_data[]) {
  uint8_t curr_sensor_lsb, curr_sensor_msb;

  for(int i,j=0; i < SENSORS_PER_FRAME; i++, j=2*i) {
    /* generate two bytes of random data */
    if (i == 0) {
      curr_sensor_msb = 0;
      curr_sensor_lsb = counter_gear % 6;
    } else if (i == 1) {
      curr_sensor_msb = counter >> 8;
      curr_sensor_lsb = 0x00FF & counter;
      counter++;
    } else {
      curr_sensor_msb = random(0x00, 0xFF);
      curr_sensor_lsb = random(0x00, 0xFF);
    }
    
    debug_print("lsb: %d", curr_sensor_lsb);
    debug_print(" msb: %d | ", curr_sensor_msb);
    /* each sensor is allocated two 
     *  bytes in the 8 byte data frame
     */
    sensor_data[j+1] = curr_sensor_lsb;
    sensor_data[j] = curr_sensor_msb;
  }
  Serial.println();
}

void p(char *fmt, ... ){
        char buf[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(buf, 128, fmt, args);
        va_end (args);
        Serial.print(buf);
}

void debug_print(char *fmt, ... ) {
  if (debug) {
     char buf[128]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(buf, 128, fmt, args);
        va_end (args);
        Serial.print(buf);
  }
}
  
